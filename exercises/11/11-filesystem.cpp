
#include "11-filesystem/api.h"
#include <cassert>
#include <vector>
#include <algorithm>


void test_filesystem()
{
    namespace fs = filesystem;

    {
        fs::SystemFacade system;

        auto folder = system.createFolder("/folder", fs::AccessRights::All);
        assert(folder);
        assert(folder.getType() == fs::EntryType::Folder);
        assert(folder.getParent());
        assert(!folder.getParent().getParent());
        assert(folder.getPath() == "/folder");
        assert(folder.getAccessRights() == fs::AccessRights::All);
        assert(folder.getChildrenCount() == 0);
    }

    {
        fs::SystemFacade system;

        auto file = system.createFile("/file", fs::AccessRights::Read, "contents");
        assert(file);
        assert(file.getType() == fs::EntryType::File);
        assert(file.getParent());
        assert(!file.getParent().getParent());
        assert(file.getPath() == "/file");
        assert(file.getAccessRights() == fs::AccessRights::Read);
        assert(file.getContents() == "contents");
    }

    {
        fs::SystemFacade system;

        auto entry = system.findEntry("/cheese");
        assert(!entry);
    }

    {
        fs::SystemFacade system;

        auto result = system.deleteEntry("/cake");
        assert(!result);
    }

    {
        fs::SystemFacade system;

        assert(system.createFolder("/pim", fs::AccessRights::All));
        assert(system.createFolder("/pim/pam", fs::AccessRights::Write));
        assert(system.createFolder("/pim/pam/poom", fs::AccessRights::Read));
        assert(system.createFile("/pim/pam/paf", fs::AccessRights::All, "something"));
        assert(system.createFile("/pouf", fs::AccessRights::Read, "nothing"));
        assert(system.createFolder("/boom", fs::AccessRights::Write));

        auto entry = system.findEntry("");
        assert(entry);
        assert(entry.getType() == fs::EntryType::Folder);
        assert(!fs::facade_cast<fs::FileFacade>(entry));
        auto folder = fs::facade_cast<fs::FolderFacade>(entry);
        assert(folder);
        assert(folder.getChildrenCount() == 3);
        std::vector<std::string> childrenPaths;
        folder.visitChildren([&childrenPaths](const auto& child) {
            childrenPaths.push_back(child.getPath());
        });
        std::sort(childrenPaths.begin(), childrenPaths.end());
        assert(childrenPaths.size() == 3);
        assert(childrenPaths[0] == "/boom");
        assert(childrenPaths[1] == "/pim");
        assert(childrenPaths[2] == "/pouf");

        entry = system.findEntry("/pim/paf");
        assert(!entry);

        entry = system.findEntry("/pim/pam");
        assert(!fs::facade_cast<fs::FileFacade>(entry));
        folder = fs::facade_cast<fs::FolderFacade>(entry);
        assert(folder);
        assert(folder.getChildrenCount() == 2);
        childrenPaths.clear();
        folder.visitChildren([&childrenPaths](const auto& child) {
            childrenPaths.push_back(child.getPath());
        });
        std::sort(childrenPaths.begin(), childrenPaths.end());
        assert(childrenPaths.size() == 2);
        assert(childrenPaths[0] == "/pim/pam/paf");
        assert(childrenPaths[1] == "/pim/pam/poom");

        entry = system.findEntry("/pim/pam/paf");
        assert(entry);
        assert(!fs::facade_cast<fs::FolderFacade>(entry));
        auto file = fs::facade_cast<fs::FileFacade>(entry);
        assert(file);
        assert(file.getContents() == "something");
        assert(file.getParent());
        assert(file.getParent().getPath() == "/pim/pam");

        assert(system.deleteEntry("/pim"));
        assert(!system.findEntry("/pim"));
        assert(!system.deleteEntry("/pim"));
        assert(system.deleteEntry("/pouf"));
        assert(!system.findEntry("/pouf"));
        assert(!system.deleteEntry("/pouf"));
    }
}
