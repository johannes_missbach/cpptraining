
#pragma once


#include <memory>
#include <string>
#include <functional>


namespace filesystem {

    namespace detail {

        class System;
        class Entry;
        using EntryPtr = Entry*;
        class Folder;
        class File;

    } // namespace detail

    enum class EntryType
    {
        Unknown = 0,
        Folder = 1,
        File = 2,
    };

    enum class AccessRights
    {
        None = 0x0,
        Read = 0x1,
        Write = 0x2,
        All = 0x3,
    };

    class EntryFacade;
    class FolderFacade;
    class FileFacade;

    class SystemFacade final
    {
    public:

        SystemFacade();

        ~SystemFacade();

        FolderFacade createFolder(const std::string& entryPath, AccessRights accessRights);

        FileFacade createFile(const std::string& entryPath, AccessRights accessRights, const std::string& contents);

        EntryFacade findEntry(const std::string& entryPath) const;

        bool deleteEntry(const std::string& entryPath);

    private:

        std::unique_ptr<detail::System> m_upImpl;
    };

    class EntryFacade
    {
        template<typename FacadeT>
        friend FacadeT facade_cast(const EntryFacade& entryFacade);

    public:

        EntryFacade(detail::EntryPtr pEntry = nullptr)
            : m_pEntry{ pEntry }
        {
        }

        explicit operator bool() const
        {
            return m_pEntry != nullptr;
        }

        EntryType getType() const;

        FolderFacade getParent() const;

        std::string getPath() const;

        AccessRights getAccessRights() const;

    protected:

        template<typename EntryPtrT>
        EntryPtrT entry_cast() const
        {
            return static_cast<EntryPtrT>(m_pEntry);
        }

    private:

        detail::EntryPtr m_pEntry = nullptr;
    };

    using EntryVisitor = std::function<void(EntryFacade)>;

    class FolderFacade final : public EntryFacade
    {
    public:

        using EntryFacade::EntryFacade;

        unsigned int getChildrenCount() const;

        void visitChildren(EntryVisitor entryVisitor) const;
    };

    class FileFacade final : public EntryFacade
    {
    public:

        using EntryFacade::EntryFacade;

        std::string getContents() const;
    };

    template<typename FacadeT>
    FacadeT facade_cast(const EntryFacade& entryFacade);

    template<>
    inline FolderFacade facade_cast(const EntryFacade& entryFacade)
    {
        if (entryFacade.getType() != EntryType::Folder)
            return FolderFacade{};
        return FolderFacade{ entryFacade.m_pEntry };
    }

    template<>
    inline FileFacade facade_cast(const EntryFacade& entryFacade)
    {
        if (entryFacade.getType() != EntryType::File)
            return FileFacade{};
        return FileFacade{ entryFacade.m_pEntry };
    }

} // namespace filesystem
