
#pragma once


#include "api.h"
#include <memory>
#include <map>
#include <algorithm>
#include <utility>


namespace filesystem { namespace detail {

    using EntryObserver = Entry*;
    using EntryOwner = std::unique_ptr<Entry>;
    using EntriesOwners = std::map<std::string, EntryOwner>;
    using FolderObserver = Folder*;
    using FolderOwner = std::unique_ptr<Folder>;

    class Entry
    {
    public:

        ~Entry() = default;

        virtual EntryType getType() const = 0;

        virtual FolderObserver getParent() const = 0;

        virtual const std::string& getName() const = 0;

        virtual std::string getPath() const = 0;

        virtual AccessRights getAccessRights() const = 0;
    };

    class EntryImpl : public Entry
    {
    public:

        EntryImpl(
            FolderObserver pParent,
            const std::string& name,
            AccessRights accessRights
            )
            : m_pParent(pParent)
            , m_name(name)
            , m_accessRights(accessRights)
        {
        }

        EntryImpl(const EntryImpl&) = delete;

        EntryImpl& operator=(const EntryImpl&) = delete;

        virtual FolderObserver getParent() const override
        {
            return m_pParent;
        }

        const std::string& getName() const
        {
            return m_name;
        }

        virtual std::string getPath() const override;

        virtual AccessRights getAccessRights() const override
        {
            return m_accessRights;
        }

    private:

        FolderObserver m_pParent;
        std::string m_name;
        AccessRights m_accessRights;
    };

    class Folder : public EntryImpl
    {
    public:

        using EntryImpl::EntryImpl;

        virtual EntryType getType() const
        {
            return EntryType::Folder;
        }

        unsigned int getChildrenCount() const
        {
            return static_cast<unsigned int>(m_children.size());
        }

        void visitChildren(EntryVisitor entryVisitor) const
        {
            std::for_each(begin(m_children), end(m_children), [&entryVisitor](const auto& child)
            {
                EntryFacade entryFacade(child.second.get());
                entryVisitor(entryFacade);
            });
        }

        EntryObserver addChild(EntryOwner child)
        {
            const auto result = m_children.emplace(child->getName(), std::move(child));
            if (!result.second)
                return EntryObserver{};
            return result.first->second.get();
        }

        EntryObserver findChild(const std::string& childName) const
        {
            const auto childIterator = m_children.find(childName);
            if (childIterator == m_children.end())
                return EntryObserver{};
            return childIterator->second.get();
        }

        bool deleteChild(const std::string& childName)
        {
            const auto childIterator = m_children.find(childName);
            if (childIterator == m_children.end())
                return false;
            m_children.erase(childIterator);
            return true;
        }

    private:

        EntriesOwners m_children;
    };

    std::string EntryImpl::getPath() const
    {
        if (m_name.empty() || !m_pParent)
            return std::string{};
        std::string path = m_pParent->getPath();
        path += '/';
        path += m_name;
        return path;
    }

    class File : public EntryImpl
    {
    public:

        File(
            FolderObserver pParent,
            const std::string& name,
            AccessRights accessRights,
            const std::string& contents
            )
            : EntryImpl(pParent, name, accessRights)
            , m_contents(contents)
        {
        }

        virtual EntryType getType() const
        {
            return EntryType::File;
        }

        std::string getContents() const
        {
            return m_contents;
        }

    private:

        std::string m_contents;
    };

    class System
    {
    public:

        EntryObserver createFolder(const std::string& entryPath, AccessRights accessRights)
        {
            const auto parentAndName = findParentAndExtractName(entryPath);
            if (parentAndName.first == nullptr || parentAndName.second.empty())
                return EntryObserver{};
            auto upFolder = std::make_unique<Folder>(parentAndName.first, parentAndName.second, accessRights);
            const auto pEntry = parentAndName.first->addChild(std::move(upFolder));
            return pEntry;
        }

        EntryObserver createFile(const std::string& entryPath, AccessRights accessRights, const std::string& contents)
        {
            const auto parentAndName = findParentAndExtractName(entryPath);
            if (parentAndName.first == nullptr || parentAndName.second.empty())
                return EntryObserver{};
            auto upFile = std::make_unique<File>(parentAndName.first, parentAndName.second, accessRights, contents);
            const auto pEntry = parentAndName.first->addChild(std::move(upFile));
            return pEntry;
        }

        EntryObserver findEntry(const std::string& entryPath) const
        {
            if (entryPath.empty())
                return m_upRoot.get();
            const auto parentAndName = findParentAndExtractName(entryPath);
            if (parentAndName.first == nullptr || parentAndName.second.empty())
                return false;
            return parentAndName.first->findChild(parentAndName.second);
        }

        bool deleteEntry(const std::string& entryPath)
        {
            const auto parentAndName = findParentAndExtractName(entryPath);
            if (parentAndName.first == nullptr || parentAndName.second.empty())
                return false;
            return parentAndName.first->deleteChild(parentAndName.second);
        }

    private:

        std::pair<FolderObserver, std::string> findParentAndExtractName(const std::string& entryPath) const
        {
            if (entryPath.empty() || entryPath[0] != '/')
                return std::make_pair(FolderObserver{}, std::string{});
            auto pParent = m_upRoot.get();
            std::string::size_type posNameBegin = 1;
            auto posNameEnd = entryPath.find('/', posNameBegin);
            while (posNameEnd != std::string::npos)
            {
                auto name = entryPath.substr(posNameBegin, posNameEnd - posNameBegin);
                auto pEntry = pParent->findChild(name);
                if (!pEntry || pEntry->getType() != EntryType::Folder)
                    return std::make_pair(FolderObserver{}, std::string{});
                pParent = static_cast<FolderObserver>(pEntry);
                posNameBegin = posNameEnd + 1;
                posNameEnd = entryPath.find('/', posNameBegin);
            }
            auto name = entryPath.substr(posNameBegin);
            return std::make_pair(pParent, name);
        }

        FolderOwner m_upRoot = std::make_unique<Folder>(nullptr, std::string{}, AccessRights::All);
    };

} } // namespace filesystem::detail
