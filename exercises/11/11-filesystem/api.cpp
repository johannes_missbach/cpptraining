
#include "api.h"
#include "implem.h"


namespace filesystem {

    SystemFacade::SystemFacade()
        : m_upImpl{ std::make_unique<detail::System>() }
    {
    }

    SystemFacade::~SystemFacade() = default;

    FolderFacade SystemFacade::createFolder(const std::string& entryPath, AccessRights accessRights)
    {
        return FolderFacade{ m_upImpl->createFolder(entryPath, accessRights) };
    }

    FileFacade SystemFacade::createFile(const std::string& entryPath, AccessRights accessRights, const std::string& contents)
    {
        return FileFacade{ m_upImpl->createFile(entryPath, accessRights, contents) };
    }

    EntryFacade SystemFacade::findEntry(const std::string& entryPath) const
    {
        return EntryFacade{ m_upImpl->findEntry(entryPath) };
    }

    bool SystemFacade::deleteEntry(const std::string& entryPath)
    {
        return m_upImpl->deleteEntry(entryPath);
    }

    EntryType EntryFacade::getType() const
    {
        if (!m_pEntry)
            return EntryType::Unknown;
        return m_pEntry->getType();
    }

    FolderFacade EntryFacade::getParent() const
    {
        if (!m_pEntry)
            return FolderFacade{};
        return FolderFacade{ m_pEntry->getParent() };
    }

    std::string EntryFacade::getPath() const
    {
        if (!m_pEntry)
            return std::string{};
        return m_pEntry->getPath();
    }

    AccessRights EntryFacade::getAccessRights() const
    {
        if (!m_pEntry)
            return AccessRights{};
        return m_pEntry->getAccessRights();
    }

    unsigned int FolderFacade::getChildrenCount() const
    {
        const auto pFolder = entry_cast<detail::Folder*>();
        if (!pFolder)
            return 0;
        return pFolder->getChildrenCount();
    }

    void FolderFacade::visitChildren(EntryVisitor EntryVisitor) const
    {
        const auto pFolder = entry_cast<detail::Folder*>();
        if (!pFolder)
            return;
        pFolder->visitChildren(EntryVisitor);
    }

    std::string FileFacade::getContents() const
    {
        const auto pFile = entry_cast<detail::File*>();
        if (!pFile)
            return std::string{};
        return pFile->getContents();
    }

} // namespace filesystem
