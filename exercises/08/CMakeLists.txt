cmake_minimum_required(VERSION 3.3)
project(exercise08)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++14")

set(SOURCE_FILES
        08.cpp)

add_executable(exercise08 ${SOURCE_FILES})