cmake_minimum_required(VERSION 3.3)
project(exercise10)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++14")

set(SOURCE_FILES
        10.cpp)

add_executable(exercise10 ${SOURCE_FILES})