
#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include <cassert>

namespace exercise1 {

    class Explicit
    {
    public:

        Explicit()
        {
            std::cout << "Default Constructor\n";
        }

        Explicit(const Explicit&)
        {
            std::cout << "Copy Constructor\n";
        }

        ~Explicit()
        {
            std::cout << "Destructor\n";
        }

        Explicit& operator=(const Explicit&)
        {
            std::cout << "Copy Assignment\n";
            return *this;
        }
    };

    void test()
    {
        Explicit exp;
        Explicit exp2(exp);
        Explicit exp3;
        exp3 = exp2;
    }
}

namespace exercise2 {

    class CanonicalCopy
    {
    public:

        CanonicalCopy() = default;

        CanonicalCopy(const CanonicalCopy& other)
            : m_s(other.m_s)
            , m_v(other.m_v)
        {
        }

        CanonicalCopy& operator=(const CanonicalCopy& other)
        {
            CanonicalCopy temp(other);
            temp.swap(*this);
            return *this;
        }

        void swap(CanonicalCopy& other)
        {
            using std::swap;
            swap(m_s, other.m_s);
            swap(m_v, other.m_v);
        }

    public:
        std::string m_s;
        std::vector<int> m_v;
    };

    void test()
    {
        CanonicalCopy cc1;
        cc1.m_s = "yop";
        cc1.m_v = { 1, 2, 3 };

        CanonicalCopy cc2;
        cc2 = cc1;
        assert(cc2.m_s == cc1.m_s);
        assert(cc2.m_v == cc1.m_v);
    }
}

int main()
{
    exercise1::test();
    exercise2::test();

    return 0;
}
