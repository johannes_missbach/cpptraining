#include <unordered_map>
#include <map>
#include <algorithm>
#include <string>
#include <iostream>

template< typename Map >
void print(const Map& map)
{
    std::cout << "raw loop:\n";
    using std::begin;
    using std::end;
    const auto endIter = end(map);
    for (auto iter = begin(map); iter != endIter; ++iter) {
        const auto& val = *iter;
        std::cout
            << "key=" << val.first
            << ",val=" << val.second << "\n";
    }

    std::cout << "std::for_each loop:\n";
    std::for_each(begin(map), end(map), [](const auto& val) {
        std::cout
            << "key=" << val.first
            << ",val=" << val.second << "\n";
    });

    std::cout << "range-based loop:\n";
    for (const auto& val : map) {
        std::cout
            << "key=" << val.first
            << ",val=" << val.second << "\n";
    }
}

void test_associative_containers()
{
    std::map<int, std::string> map
    {
        { 1, "one" },
        { 2, "two" },
        { 3, "three" },
        { 4, "four" },
        { 5, "five" }
    };
    print(map);

    std::unordered_map<std::string, int> umap
    {
        { "one", 1 },
        { "two", 2 },
        { "three", 3 },
        { "four", 4 },
        { "five", 5 }
    };
    print(umap);
}
