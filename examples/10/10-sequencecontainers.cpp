#include <array>
#include <vector>
#include <deque>
#include <list>
#include <string>
#include <algorithm>
#include <iostream>

template< typename Seq >
void print(const Seq& seq)
{
    std::cout << "raw loop:\n";
    using std::begin;
    using std::end;
    const auto endIter = end(seq);
    for (auto iter = begin(seq); iter != endIter; ++iter) {
        const auto& val = *iter;
        std::cout << "val=" << val << "\n";
    }

    std::cout << "std::for_each loop:\n";
    std::for_each(begin(seq), end(seq), [](const auto& val) {
        std::cout << "val=" << val << "\n";
    });

    std::cout << "range-based loop:\n";
    for (const auto& val : seq) {
        std::cout << "val=" << val << "\n";
    }
}

void test_sequence_containers()
{
    std::array<int, 5> cppArr{ 1, 2, 3, 4, 5 };
    print(cppArr);

    int cArr []{ 1, 2, 3, 4, 5 };
    print(cArr);

    std::vector<int> vec{ 1, 2, 3, 4, 5 };
    print(vec);

    std::deque<int> deq{ 1, 2, 3, 4, 5 };
    print(deq);

    std::list<int> lst{ 1, 2, 3, 4, 5 };
    print(lst);

    std::string s{ "hello" };
    print(s);
}
