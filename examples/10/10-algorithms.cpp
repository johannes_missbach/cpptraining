
#include <vector>
#include <algorithm>
#include <string>
#include <iostream>

void test_algorithms()
{
    std::vector<std::string> vec{ "mouse", "cat", "dog", "pig", "monkey" };

    const auto catIter = std::find(vec.begin(), vec.end(), "cat");
    if (catIter != vec.end())
        std::cout << "Found cat\n";

    std::sort(vec.begin(), vec.end());
    std::cout << "Vec is now sorted:\n";
    for (const auto& animal : vec)
        std::cout << animal << "\n";

    const auto numberOfManimals = std::count_if(vec.begin(), vec.end(), [](const auto& animal) {
        return !animal.empty() && animal[0] == 'm';
    });
    std::cout << "Numbfer of animals starting with m = " << numberOfManimals << "\n";
}
