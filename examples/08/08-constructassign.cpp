#include <cassert>
#include <string>

class Mover
{
public:

    Mover() = default;

    Mover(const Mover&) = delete;

    Mover(Mover&& mover)
        : m_s{ std::move(mover.m_s) }
    {
    }

    explicit Mover(std::string&& s)
        : m_s{ std::move(s) }
    {
    }

    Mover& operator=(const Mover&) = delete;

    Mover& operator=(Mover&& mover)
    {
        m_s = std::move(mover.m_s);
        return *this;
    }

    std::string m_s;
};

void test_constructassign()
{
    Mover mover1(std::string("bonjour"));

    Mover mover2(std::move(mover1));

    Mover mover3;
    mover3 = std::move(mover2);
}
