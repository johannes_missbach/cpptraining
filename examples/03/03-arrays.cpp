
#include <array>
#include <string>
#include <cassert>

template<typename ElemType, std::size_t Size >
constexpr std::size_t getArraySize(ElemType(&arr)[Size]) {
    return Size;
}

void arraysAndStrings() {
    // Array of size 5
    int c_array[5] = {};
    std::size_t arraySize = sizeof(c_array) / sizeof(c_array[0]);
    assert(arraySize == 5);
    assert(arraySize == getArraySize(c_array));

    // Array of deduced size 3
    int other_c_array [] = { 1, 2, 3 };

    // prefer using
    std::array<int, 5> cpp_array = {};

    // Pointers and arrays are similar
    int* pointer = other_c_array;
    assert(pointer == other_c_array);

    // Array of size 6
    // String of length 5
    char c_tring [] = "hello";

    // String of wide characters
    wchar_t other_c_string [] = L"hello";

    // prefer using
    std::string cpp_string = "hello";
}
