
#include <cassert>

void test() {
    int someVar = 123;

    // i is a reference to someVar
    int& i = someVar;
    // i and smeVar share the same address for ever
    assert(&i == &someVar);
    // i and smeVar share the same value for ever
    assert(someVar == i && i == 123);

    // when i is modified, indeed it modifies someVar's value
    ++i;
    assert(someVar == i && i == 124);

    // j is a local variable,
    // which initial value is same as i
    int j = i;
    // initially i and j share the same value
    assert(j == i && i == 124);
    // i and j do not have the same address
    assert(&j != &i);

    // when j is modified it does not affect i
    j += 10;
    assert(j == 134 && i == 124 && someVar == 124);

    // otherRef is a reference to i
    int& otherRef = i;

    // when j is assigned to otherRef,
    // it does not re-bind otherRef to j
    // indeed it modifies i's value and someVar's value
    otherRef = j;
    assert(someVar == i && someVar == otherRef && someVar == 134);
}

int globVar = 789;

int& getGlobRef() {
    // return a reference to a global variable. OK
    return globVar;
}

int& getLocalStaticRef() {
    static int i = 1;
    // return a reference to a static local variable. OK
    return i;
}

int& getLocalRef() {
    int i = 2;
    // return a reference to a local variable. BAD !!!!
    return i;
}

void getRefs() {
    int& r1 = getGlobRef();
    assert(&r1 == &globVar && r1 == globVar && r1 == 789);
    int& r2 = getLocalStaticRef();
    int& r3 = getLocalStaticRef();
    assert(&r2 == &r3 && r2 == r3 && r2 == 1);
    int& r4 = getLocalRef();
    assert(r4 == 2); // OUCH, unpredictable result !!!
}
