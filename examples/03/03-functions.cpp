
#include <string>
#include <vector>
#include <cassert>

void add(int a, int b, int* pResult)
{
    *pResult = a + b;
}

std::size_t getLength1(std::string s)
{
    return s.length();
}

std::size_t getLength2(const std::string& s)
{
    return s.length();
}

std::vector<double> makeV(double val, std::size_t& len)
{
    std::vector<double> v = { 1.2, 2.3, 3.4 };
    v.push_back(val);
    len = v.size();
    return v;
}

void useThemAll()
{
    int result = 0;
    add(3, 5, &result);
    assert(result == 8);

    std::string str("impressive");
    const std::size_t length1 = getLength1(str);
    const std::size_t length2 = getLength2(str);
    assert(length1 == length2 && length1 == 10);

    std::size_t theLength = 0;
    const std::vector<double> vec = makeV(4.5, theLength);
    assert(theLength == 4 && theLength == vec.size());
    assert(vec[0] == 1.2);
    assert(vec[1] == 2.3);
    assert(vec[2] == 3.4);
    assert(vec[3] == 4.5);
}
