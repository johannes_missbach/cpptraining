
#include <cassert>

static double d = 12.415;

// pD is a pointer to d
// its value is the address of d
static double* pD = &d;

int func(char c)
{
    return c;
}

// FuncType is an alias
// to a function type
// which returns an int
// and takes a char as parameter
using FuncType = int(char);

// pFunc is a pointer to a function of type FuncType
// It is initialized with the address of func
FuncType* pFunc = &func;

void nothingInteresting()
{
    // Calling func indirectly through the pointer pFunc
    (*pFunc)('A');

    // a c-string is a c-array of chars, zero-terminated
    const char c_str [] = "hello berlin";

    // A pointer can be constructed from a C-array
    // then it points to its first element
    const char* p = c_str;

    // A pointer can be dereferenced with operator*
    // We can take the value of the pointed object
    assert(*p == 'h');

    // We can do pointer arithmetics
    ++p;
    p = p + 7;
    p -= 2;

    const char c = *p;
    assert(*p == 'b');
}
