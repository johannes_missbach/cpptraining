
void test();
void getRefs();
void nothingInteresting();
void useThemAll();
void arraysAndStrings();

int main() {
    test();
    getRefs();
    nothingInteresting();
    useThemAll();
    arraysAndStrings();
}
