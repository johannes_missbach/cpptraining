
#include <array>
#include <algorithm>
#include <iostream>

template< typename T, unsigned M, unsigned N >
class Matrix
{
public:

    Matrix() = default;

    Matrix(std::initializer_list<T> values)
    {
        const unsigned inputValuesCount = static_cast<unsigned>(std::distance(values.begin(), values.end()));
        const auto maxValuesCount = std::min(inputValuesCount, Size);
        std::copy_n(values.begin(), maxValuesCount, m_values.begin());
        std::fill(m_values.begin() + maxValuesCount, m_values.end(), 0);
    }

    template< typename OutputStreamT >
    void print(OutputStreamT& outputStream) const
    {
        for (unsigned row = 0; row != M; ++row)
        {
            for (unsigned col = 0; col != N; ++col)
            {
                outputStream << " ";
                outputStream << item(row, col);
            }
            outputStream << "\n";
        }
    }

    const T& item(unsigned row, unsigned col) const
    {
        return m_values[row*N + col];
    }

    T& item(unsigned row, unsigned col)
    {
        return m_values[row*N + col];
    }

private:
    
    static constexpr unsigned Size = M*N;

    std::array<T, Size> m_values;
};

template<typename T, unsigned M, unsigned N, unsigned P>
Matrix<T, M, P> operator*(const Matrix<T, M, N>& m1, const Matrix<T, N, P>& m2)
{
    Matrix<T, M, P> result{};
    for (unsigned p = 0; p != P; ++p)
    {
        for (unsigned m = 0; m != M; ++m)
        {
            T sum = 0;
            for (unsigned n = 0; n != N; ++n)
            {
                sum += m1.item(m, n) * m2.item(n, p);
            }
            result.item(m, p) = sum;
        }
    }
    return result;
}

void test_classtemplates()
{
    std::cout << "m1=\n";
    Matrix<int, 3, 2> m1{ 1, 2, 3, 4, 5, 6 };
    m1.print(std::cout);

    std::cout << "m2=\n";
    Matrix<int, 2, 4> m2{ 10, 20, 30, 40, 50, 60, 70, 80 };
    m2.print(std::cout);

    std::cout << "m1*m2=\n";
    auto m = m1 * m2;
    m.print(std::cout);
}
