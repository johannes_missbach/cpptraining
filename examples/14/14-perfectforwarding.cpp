
#include <string>
#include <vector>

struct Toto
{
    Toto(const Toto&) = default;
    Toto(Toto&&) = default;
    explicit Toto(int i) {}
    explicit Toto(const std::vector<int>& v) {}
    explicit Toto(std::vector<double>& v) {}
    explicit Toto(std::string&& s) {}
};

template< typename AnyT >
Toto makeToto_unperfect(const AnyT& arg)
{
    Toto toto(arg);
    return toto;
}

template< typename AnyT >
Toto makeToto_unperfect(AnyT& arg)
{
    Toto toto(arg);
    return toto;
}

template< typename AnyT >
Toto makeToto_perfect(AnyT&& arg)
{
    Toto toto(std::forward<AnyT>(arg));
    return toto;
}

void test_perfectforwarding()
{
    auto toto1 = makeToto_perfect(123);
    auto toto2 = makeToto_perfect(std::vector<int>{1, 2, 3});
    std::vector<double> v{1.2, 2.3, 3.4};
    auto toto3 = makeToto_perfect(v);
    auto toto4 = makeToto_perfect(std::string("123"));
}
