
#include <type_traits>
#include <cstdio>
#include <cassert>
#include <string>

template< typename T >
typename T::DependentType1 basic_sfinae(T t)
{
    printf("Type containing DependentType1\n");
    return 0;
}

template< typename T >
typename T::DependentType2 basic_sfinae(T t)
{
    printf("Type containing DependentType2\n");
    return 0;
}

template<bool B, class T = void>
struct enable_if {};

template<class T>
struct enable_if<true, T> { typedef T type; };

template< typename T >
typename std::enable_if<std::is_integral<T>::value, std::string>::type enableif_sfinae(T t)
{
    printf("t is integral\n");
    return "integral";
}

template< typename T >
typename std::enable_if<std::is_class<T>::value, int>::type enableif_sfinae(const T& t)
{
    printf("t is class\n");
    return 2;
}


void test_sfinae()
{
    {
        struct S1
        {
            using DependentType1 = int;
        };

        struct S2
        {
            using DependentType2 = char;
        };

        basic_sfinae(S1{});
        basic_sfinae(S2{});
    }

    {
        const auto result = enableif_sfinae(53);
        assert(result == "integral");
    }

    {
        class Toto {};
        const auto result = enableif_sfinae(Toto{});
        assert(result == 2);
    }
}
