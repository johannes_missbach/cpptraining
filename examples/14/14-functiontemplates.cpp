
#include <iostream>
#include <string>

template< typename OutputStreamT, typename T >
void xmlPrintToStream(OutputStreamT& outputStream, const T& anything, const char* xmlElementName)
{
    outputStream << "<" << xmlElementName << ">";
    outputStream << anything;
    outputStream << "</" << xmlElementName << ">\n";
}

template< typename T >
void xmlPrint(const T& anything)
{
    xmlPrintToStream(std::cout, anything, "anything");
}

template<>
void xmlPrint<std::string>(const std::string& s)
{
    xmlPrintToStream(std::cout, s, "spec-string");
}

void xmlPrint(const std::string& s)
{
    xmlPrintToStream(std::cout, s, "string");
}

void xmlPrint(int i)
{
    xmlPrintToStream(std::cout, i, "int");
}

void test_functiontemplates()
{
    xmlPrint(14.56);
    xmlPrint(123);
    xmlPrint("hello");
    xmlPrint(std::string("hello"));
    xmlPrint<std::string>(std::string("hello"));
    xmlPrint<std::string>("hello");
}
