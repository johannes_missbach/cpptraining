#include <cstdint>

long long int big = 1234567890123456789;
short small = static_cast<short>(big);
short small2 = (short) big;
short small3 = short(big);

const int c = 123;
int& nc = const_cast<int&>(c);

class Base
{
    virtual void F() {}
};
class Derived : public Base
{
};
Derived derived;
Base* pBase = &derived;
Derived* pDerived = dynamic_cast<Derived*>(pBase);

std::intptr_t number = 0xABCDEF;
void* pointer = reinterpret_cast<void*>(number);
