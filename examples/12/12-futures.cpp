
#include <future>
#include <chrono>
#include <iostream>
#include <sstream>
#include <string>
#include <ctime>
#include <iomanip>

using namespace std::literals;

void log(const std::string& text);

void taskProc()
{
    log("Entering Task Proc");
    std::this_thread::sleep_for(2s);
    log("Leaving Task Proc");
}

struct TaskFunc
{
    int operator()(int a, int b) const
    {
        log("Entering TaskFunc Operator()");
        std::this_thread::sleep_for(1s);
        auto result = a * b;
        log("Leaving TaskFunc Operator()");
        return result;
    }
};

void test_futures()
{
    log("Launching task 1");
    auto fut1 = std::async(
        std::launch::async, taskProc);

    log("Launching task 2 (computing 5 * 8)");
    auto fut2 = std::async(
        std::launch::async, TaskFunc(), 5, 8);

    log("Waiting for task 2 to finish");
    auto result = fut2.get();
    log(std::string("Result of task 2 is ")
        + std::to_string(result));

    log("Waiting for task 1 to finish");
    fut1.wait();
}
