
#include <thread>
#include <mutex>
#include <condition_variable>
#include <atomic>
#include <chrono>
#include <iostream>
#include <sstream>
#include <string>
#include <ctime>
#include <iomanip>

using namespace std::literals;

void log(const std::string& text)
{
    static std::mutex s_mutex;
    std::lock_guard<std::mutex> guard(s_mutex);
    const auto now = std::chrono::system_clock::now();
    const auto time = std::chrono::system_clock::to_time_t(now);
    std::stringstream timeStream;
    timeStream << std::put_time(std::localtime(&time), "%Y-%m-%d %X");
    std::cout << timeStream.str() << ":";
    std::cout << std::this_thread::get_id() << ":";
    std::cout << text;
    std::cout << std::endl;
}

static std::atomic<bool> g_stop = false;

void threadProc()
{
    log("Entering Thread Proc");
    while (!g_stop);
    log("Leaving Thread Proc");
}

struct ThreadFunc
{
    void operator()() const
    {
        log("Entering ThreadFunc Operator()");
        {
            std::unique_lock<std::mutex> lock(m_mut);
            m_cv.wait(lock);
        }
        log("Leaving ThreadFunc Operator()");
    }

    mutable std::mutex m_mut;
    mutable std::condition_variable m_cv;
};

void test_threads()
{
    log("Launching th1");
    std::thread th1(threadProc);

    log("Launching th2");
    ThreadFunc threadFunc;
    std::thread th2(std::cref(threadFunc));

    log("Doing something else in the main thread");
    std::this_thread::sleep_for(2s);

    log("Telling th1 to finish");
    g_stop = true;

    log("Telling th2 to finish");
    threadFunc.m_cv.notify_one();

    log("Waiting for th2 to finish");
    th2.join();
    log("Waiting for th1 to finish");
    th1.join();
}
