
namespace meine_name_platz {
    // dankeSchon has namespace scope
    float dankeSchon;
}

// func has global namespace scope
// param has function scope
unsigned int func(unsigned int param) {

    // result has block scope
    unsigned int result = 101;

    if (param < 10) {

        // square has block scope
        unsigned int square = param * param;

        result = square;
    }

    return result;
}

namespace foo { namespace boom {

    // C has namespace scope
    class C {

        // memberFunc has class scope
        void memberFunc();

        // member has class scope
        char member;

        // otherMember has class scope
        static double otherMember;
    };

} }
