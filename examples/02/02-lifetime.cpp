
// longestLife lifetime begins at program beginning
// and ends at program end
long long longestLife;

class Cla {
    // memberLife lifetime begins at Cla object's construction
    // and ends at Cla object's destruction
    char memberLife;
};

void func() {
    // longLife lifetime begins when the first time func is called
    // and ends at program end
    static int longLife;

    // Lifetime of object pointed-to by pCla begins at the new instruction
    // and ends at the delete instruction
    Cla* pCla = new Cla();

    {
        // smallLife lifetime begins at its declaration
        // and ends at the first closing bracket
        int smallLife;
    }

    delete pCla;
}
