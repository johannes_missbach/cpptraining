
int i; // global object initialized to 0

static double f; // global object initialized to 0.0

char* p; // global object initialized to nullptr

long arr[15]; // global array initialized with zeros

void fun()
{
    int i; // local object, uninitialized !!!
    static double f; // global object initialized to 0.0
    char* p; // local object uninitialized !!!
    long arr[15]; // local array, uninitialized !!!
}

class T
{
    unsigned int m_i; // member object, uninitialized !!!
    static float m_f; // global object declaration
};

float T::m_f; // global object definition, initialized to 0.0f
