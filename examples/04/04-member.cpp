
#include <string>

class Klasse
{
public:
    int m_i{};
    float m_f = 87.4f;
    std::string m_s{ "boo" };
};

class Klasse2
{
public:
    Klasse2()
        : m_i(78)
        , m_f{ 45.f }
        , m_s()
    {
    }

    int m_i;
    float m_f;
    std::string m_s;
};
