#include<string>
#include<vector>

auto n = 0; // type of n is deduced as int

auto s = std::string{ "hello" }; // type of s is deduced as std::string

const double& F() {
    static double d = 6.5;
    return d;
}
auto d = F(); // type of d is deduced as double

char c;
auto& c2 = c; // type of c2 is deduced as char&

std::vector<int> vec;
const auto count = vec.size(); // type of count is deduced as const std::size_t
