#include <string>
#include <iostream>

void notVeryUseful()
{
    // not very useful but possible
    std::string("hello"); // constructed here
                          // destroyed here

    // more useful:
    // a const reference extends the lifetime
    // of a temporary object until the reference dies
    const std::string& s =
        std::string("gutentag"); // temporary unnamed object

    // s references the not-so-temporary object
    // so it can still be accessed
    std::cout << s;
} // the temporary object "gutentag" dies here

void F(const std::string& s)
{
    std::cout << s;
}

void moreUseFul()
{
    // temporary unnamed object
    // passed as an argument
    F(std::string("hello"));

    // temporary unnamed object
    // implicitely created by the compiler
    F("hello");

    // same effect as
    std::string s("hello");
    F(s);
}
