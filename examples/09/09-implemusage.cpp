class Sum
{
public:

    void operator() (const int& element)
    {
        m_sum += element;
    }

    int get() const
    {
        return m_sum;
    }

private:

    int m_sum = 0;
};

#include <vector>
#include <cassert>

void my_for_each(std::vector<int>& vec, Sum& sum)
{
    for (const auto& elem : vec)
        sum(elem);
}

void use()
{
    std::vector<int> vec{ 1, 2, 3, 4 };
    Sum sum;
    my_for_each(vec, sum);
    assert(sum.get() == 10);
}
