
void testdefaultconstruct();
void testcopyconstruct();
void testcopyassignment();
namespace test1 { void testinheritance(); }
namespace test2 { void testinheritance(); }
void testinterface();

int main()
{
    testdefaultconstruct();
    testcopyconstruct();
    testcopyassignment();
    test1::testinheritance();
    test2::testinheritance();
    testinterface();
    return 0;
}
