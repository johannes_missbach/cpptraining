#include <string>
#include <cassert>

class C
{
public:

    const std::string& get() const
    {
        return m_s;
    }

    void set(const std::string& s)
    {
        m_s = s;
    }

private:

    std::string m_s;
};

void testcopyconstruct()
{
    C c;
    c.set("warum weil");

    C c2(c); // Copy construction
    assert(c2.get() == "warum weil");

    C c3 = c; // Copy construction too
    assert(c3.get() == "warum weil");
}
