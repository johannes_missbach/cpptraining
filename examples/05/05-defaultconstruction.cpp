
#include <cassert>

struct C
{
    // Default constructor
    // with compiler-default implementation
    C() = default;

    // Some other constructor
    C(int i);

    int m_i;
    int m_k;
};

// Definition of the other constructor
// m_i and m_j are initialized with the value of i
C::C(int i)
    : m_k(i)
    , m_i(i)
{
}

void testdefaultconstruct()
{
    C c1(78);
    assert(c1.m_i == 78); // predictable behavior

    C c2{};
    assert(c2.m_i == 0); // predicatable behavior

    C c3; // c3's members are uninitialized
    // not a problem as long as we don't read them

    C c4;
    assert(c4.m_i == 0xcccccccc); // OUCH: undefined behavior !!!
}
