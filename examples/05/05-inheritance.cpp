
#include <cassert>
#include <string>

namespace test1 {

    class Base1
    {
    public:
        std::string F1() { return "F1()"; }
    };

    class Base2
    {
    public:
        std::string F2() { return "F2()"; }
    };

    class Derived : public Base1, private Base2
    {
    public:
        std::string F3() { return "F3()"; }
    };

    void testinheritance()
    {
        Derived derived;
        assert(derived.F1() == "F1()");

        // Compiler error: private function
        // assert(derived.F2() == "F2()");

        assert(derived.F3() == "F3()");
    }

}

namespace test2 {

    class Base
    {
    public:
        std::string F1()
        {
            return "Base::F1()";
        }

        virtual std::string F2()
        {
            return "Base::F2()";
        }
    };

    class Derived : public Base
    {
    public:
        std::string F1()
        {
            return "Derived::F1()";
        }

        virtual std::string F2() override
        {
            return "Derived::F2()";
        }
    };

    void testinheritance()
    {
        Derived derived;
        assert(derived.F1() == "Derived::F1()");
        assert(derived.F2() == "Derived::F2()");

        Base base;
        assert(base.F1() == "Base::F1()");
        assert(base.F2() == "Base::F2()");

        Base& baseref = derived;
        assert(baseref.F1() == "Base::F1()");
        assert(baseref.F2() == "Derived::F2()");

        Base* baseptr = &derived;
        assert(baseptr->F1() == "Base::F1()");
        assert(baseptr->F2() == "Derived::F2()");
    }

}
